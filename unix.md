## General
```
df
du -shc *
```

## Find
```
# Delete all file before certain date 
find . -maxdepth 1 -mtime -1  -type f  -printf "%M %-6u %-6g  %-${3:-40}p %TY-%Tm-%Td %TH:%TM\n"
find . -maxdepth 1 -mtime -1 -type f -name "*" -delete
# Delet all file from yesterday only
find ./ -newermt $(date +"%Y-%m-%d" -d "yesterday") ! -newermt $(date +"%Y-%m-%d") -delete

```


## Exoscale

### connect ssh
```
exo compute instance list

# List only machine names
exo compute instance list -O json | jq ".[] | .name"

exo compute instance ssh jumphost -o "-i ~/.ssh/id_rsa.smv2"
```

## K8s
```
# attach shell to tty
kubectl get namespaces
kubectl get pods -n prod-covid-vac-2
kubectl exec -i -t -n {namespace} {podname} -- sh
```




