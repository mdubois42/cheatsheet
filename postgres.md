## quick command

```
\c <database>       # Change database
\l                  # Show databases
\du+                # Show users
\dt                 # Show all tables
\d <tablename>      # Describe table
\i <filepath>       # execute script
\?                  # Show help
```


## Get users schemas

```
SELECT schema_name
FROM information_schema.schemata;
```

### Create user 
```
sudo -u postgres psql
postgres=# create database mydb;
postgres=# create user myuser with encrypted password 'mypass';
postgres=# grant all privileges on database mydb to myuser;
```

### Get Database ownershi
```sql
SELECT d.datname as "Name",
pg_catalog.pg_get_userbyid(d.datdba) as "Owner"
FROM pg_catalog.pg_database d;
```

## Backup

```
docker exec -t your-db-container pg_dumpall -c -U postgres > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql
docker exec -it 971bb55a6610 pg_dump -U flowable -F t -d flowable-sm > flowable_sm.tar #Tar 

 pg_restore -Ft -d 'postgres://flowable:PASSWORD@HOST/flowable_uat?sslmode=require' flowable_sm.tardump
```

## invalid primary checkpoint record 
```
docker run -it  --mount type=bind,source="$(pwd)"/flowable_data-postgres-flowable,target=/var/lib/postgresql/data postgres:13.3 /bin/bash
gosu postgres pg_resetwal /var/lib/postgresql/data
--> Write-ahead log reset

# Please make a backup before executing this command if needed
pg_resetwal -f /var/lib/postgresql/data

```

## Change object ownership
```sql
ALTER DATABASE my_datagbase OWNER TO my_user;

SELECT 'ALTER TABLE '|| 'public' || '."' || tablename ||'" OWNER TO user_rule_engine_staging;'
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog' AND 
    schemaname != 'information_schema';

SELECT 'ALTER SEQUENCE '|| sequence_schema || '."' || sequence_name ||'" OWNER TO user_rule_engine;'
FROM information_schema.sequences WHERE NOT sequence_schema IN ('pg_catalog', 'information_schema')
ORDER BY sequence_schema, sequence_name;

SELECT 'ALTER VIEW '|| table_schema || '."' || table_name ||'" OWNER TO my_new_owner;'
FROM information_schema.views WHERE NOT table_schema IN ('pg_catalog', 'information_schema')
ORDER BY table_schema, table_name;

```

